//
//  HeaderNewLessonTableViewCell.swift
//  timeTable
//
//  Created by Павел Москалев on 10/6/21.
//

import UIKit

class HeaderTableViewCell : UITableViewHeaderFooterView {
    
    var headerLabel = UILabel(text: "HEADER", font: .SFProText20(), textAlignment: .left)
    

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        headerLabel.textColor = .black
        self.contentView.backgroundColor = .systemTeal

        setConstraints()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setConstraints() {
        
        self.addSubview(headerLabel)
        NSLayoutConstraint.activate([
                                        headerLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 25),
                                        headerLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5)])
    }
    
    func prepareHeader( nameArray : [String], section: Int) {
        headerLabel.text = nameArray[section]
    }
}
