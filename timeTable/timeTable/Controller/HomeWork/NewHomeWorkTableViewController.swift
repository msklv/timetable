//
//  NewHomeWorkTableViewController.swift
//  timeTable
//
//  Created by Павел Москалев on 10/8/21.
//

import UIKit

class NewHomeWorkTableViewController: UITableViewController{
    
    let idNewHomeWorkCell = "idNewLessonCell"
    let idNewHomeWorkHeader = "idNewLessonHeader"
    
    var headerNameArray = ["Дата","Название","Задание","Цвет"]

    override func viewDidLoad () {
        super.viewDidLoad()
        title = "New HomeWork"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .systemTeal
        tableView.separatorStyle = .none
        tableView.register(NewHomeWorkTableViewCell.self, forCellReuseIdentifier: idNewHomeWorkCell)
        tableView.register(HeaderTableViewCell.self, forHeaderFooterViewReuseIdentifier: idNewHomeWorkHeader)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: idNewHomeWorkCell ,for: indexPath) as! NewHomeWorkTableViewCell
        cell.prepareCell(indexPath: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: idNewHomeWorkHeader) as! HeaderTableViewCell
        header.prepareHeader(nameArray: headerNameArray, section: section)
        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! NewHomeWorkTableViewCell
        
        switch indexPath.section {
        case 0: alertDate(label: cell.nameCellLabel) { (numberOfWeekDay, data) in
            print(numberOfWeekDay, data)
        }
        case 1: alertWithText(label: cell.nameCellLabel, name: "Введите название предмета", placeholder: "")
        case 2: alertWithText(label: cell.nameCellLabel, name: "Введите задание", placeholder: "")
        case 3:
            let colors = LessonColorViewController()
            navigationController?.pushViewController(colors, animated: true)
        default:
            print("tap options tableView")
        }
    }
}
