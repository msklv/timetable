//
//  ViewController.swift
//  timeTable
//
//  Created by Павел Москалёв on 18.08.21.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavBar()
    }
    
    func setUpNavBar () {
        let timeTableViewController = createNavController(vc: TimeTableViewController(), itemName: "TimeTable", itemImage: "calendar.badge.clock")
        let homeWorkViewController = createNavController(vc: HomeWorkViewController(), itemName: "HomeWork", itemImage: "text.badge.checkmark")
        let contactsViewController = createNavController(vc: ContactsTableViewController(), itemName: "Contacts", itemImage: "rectangle.stack.person.crop")
        
        viewControllers = [ timeTableViewController , homeWorkViewController , contactsViewController]
    }
    
    func createNavController (vc: UIViewController , itemName: String , itemImage : String) -> UINavigationController
    {
        let item = UITabBarItem(title: itemName, image: UIImage(systemName: itemImage)?.withAlignmentRectInsets(.init(top: 0, left: 0, bottom: 0, right: 0)), tag: 0)
        item.titlePositionAdjustment = .init(horizontal: 0, vertical: 0)
        
        let navController = UINavigationController(rootViewController: vc)
        navController.tabBarItem = item
        return navController
    }
    
}

