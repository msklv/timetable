//
//  LessonColorViewController.swift
//  timeTable
//
//  Created by Павел Москалев on 10/7/21.
//

import UIKit

class LessonColorViewController: UITableViewController{
    
    let idNewColorCell = "idNewLessonCell"
    let idNewLessonHeader = "idNewLessonHeader"
    
    var headerNameArray = ["RED","ORANGE","YELLOW","GREEN","BLUE","DEEP BLUE","PURPLE"]

    override func viewDidLoad () {
        super.viewDidLoad()
        title = "Выбери цвет"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .systemTeal
        tableView.separatorStyle = .none
        tableView.register(ColorLessonTableViewCell.self, forCellReuseIdentifier: idNewColorCell)
        tableView.register(HeaderTableViewCell.self, forHeaderFooterViewReuseIdentifier: idNewLessonHeader)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: idNewColorCell ,for: indexPath) as! ColorLessonTableViewCell
        cell.prepareCell(indexPath: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: idNewLessonHeader) as! HeaderTableViewCell
        header.prepareHeader(nameArray: headerNameArray, section: section)
        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
}



