//
//  TimeTableViewController.swift
//  timeTable
//
//  Created by Павел Москалёв on 18.08.21.
//

import UIKit
import FSCalendar

class TimeTableViewController: UIViewController {
    
    var calendarHeightConstraints : NSLayoutConstraint?
    
    private var calendar : FSCalendar = {
        let calendar = FSCalendar()
        calendar.translatesAutoresizingMaskIntoConstraints = false
        calendar.scope = .week
        calendar.firstWeekday = 2
        calendar.locale = Locale(identifier: "ru_RU")
        return calendar
    }()
    
    let showHideButton : UIButton =  {
        let button = UIButton()
        button.setTitle("Show calendar", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont(name: "Avenir Next Demi Bolt", size: 14)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let tableView : UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.bounces = false
        return tableView
    }()
    
    let idTimeTableCell = "timeTableCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "TimeTable"
        calendar.delegate = self
        calendar.dataSource = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(TimeTableViewCell.self, forCellReuseIdentifier: idTimeTableCell)
        setConstrains()
        swipeAction()
        showHideButton.addTarget(self, action: #selector(showHideButtonTapped), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonTapped))
    }
    
    
    @objc func addButtonTapped() {
        let newLesson = AddNewLessonTableViewController()
        navigationController?.pushViewController(newLesson, animated: true)
        
    }
    
    
    @objc func showHideButtonTapped () {
        if calendar.scope == .week
        {
            calendar.setScope(.month, animated: true)
            showHideButton.setTitle("Close calendar", for: .normal)
        }
        else
        {
            calendar.setScope(.week, animated: true)
            showHideButton.setTitle("Show calendar", for: .normal)
        }
    }
    
    //MARK:- SwipeGestureRecognizer
    
    func swipeAction() {
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        swipeUp.direction = .up
        calendar.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        swipeDown.direction = .down
        calendar.addGestureRecognizer(swipeDown)
    }
    
    @objc func handleSwipe(gesture: UISwipeGestureRecognizer) {
        switch gesture.direction {
        case .up:
            showHideButtonTapped()
        case .down:
            showHideButtonTapped()
        default:
            break
        }
    }
}
//MARK:- TableViewDelegate/DataSource

extension TimeTableViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: idTimeTableCell, for: indexPath) as! TimeTableViewCell
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

//MARK:- FSCalendarDelagate , FSCalendarDataSource

extension TimeTableViewController : FSCalendarDelegate , FSCalendarDataSource {
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        calendarHeightConstraints?.constant = bounds.height
        view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print(date)
    }
}

//MARK:- Extension setConstrains

extension TimeTableViewController {
    
    func setConstrains() {
        view.addSubview(calendar)
        calendarHeightConstraints = NSLayoutConstraint.init(item: calendar, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 300)
        calendar.addConstraint(calendarHeightConstraints!)
        NSLayoutConstraint.activate([
                                        calendar.topAnchor.constraint(equalTo: view.topAnchor, constant: 60),
                                        calendar.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
                                        calendar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0)])
        
        view.addSubview(showHideButton)
        NSLayoutConstraint.activate([
                                        showHideButton.topAnchor.constraint(equalTo: calendar.bottomAnchor, constant: 0),
                                        showHideButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15)])
        
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
                                        tableView.topAnchor.constraint(equalTo: showHideButton.bottomAnchor, constant: 10),
                                        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
                                        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
                                        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),])
    }
}

