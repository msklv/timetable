//
//  AddNewLessonViewController.swift
//  timeTable
//
//  Created by Павел Москалев on 10/5/21.
//

import UIKit

class AddNewLessonTableViewController: UITableViewController{
    
    let idNewLessonCell = "idNewLessonCell"
    let idNewLessonHeader = "idNewLessonHeader"
    
    var headerNameArray = ["Дата и время","Предмет","Преподаватель","Цвет","Период"]

    override func viewDidLoad () {
        super.viewDidLoad()
        title = "New lesson"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .systemTeal
        tableView.separatorStyle = .none
        tableView.register(AddNewLessonTableViewCell.self, forCellReuseIdentifier: idNewLessonCell)
        tableView.register(HeaderTableViewCell.self, forHeaderFooterViewReuseIdentifier: idNewLessonHeader)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 4
        case 2:
            return 1
        case 3:
            return 1
        default:
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: idNewLessonCell ,for: indexPath) as! AddNewLessonTableViewCell
        cell.prepareCell(indexPath: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: idNewLessonHeader) as! HeaderTableViewCell
        header.prepareHeader(nameArray: headerNameArray, section: section)
        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! AddNewLessonTableViewCell
        
        switch indexPath {
        case [0,0]:
            alertDate(label: cell.nameCellLabel) { (numberWeekDay, date) in
                print(numberWeekDay,date)
            }
        case [0,1]:
            alertTime(label: cell.nameCellLabel) { (date) in
                print(date)
            }
            
        case [1,0],[1,1],[1,2],[1,3]:
            alertWithText(label: cell.nameCellLabel, name: cell.textFieldAlertArray[indexPath.section][indexPath.row] , placeholder: cell.cellNameArray[indexPath.section][indexPath.row])
        case [2,0]:
            let teachers = TeachersViewController()
            navigationController?.pushViewController(teachers, animated: true)
        case [3,0]:
            let colors = LessonColorViewController()
            navigationController?.pushViewController(colors, animated: true)
            
        default:
            print("tap options tableView")
        }
    }
}


