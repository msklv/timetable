//
//  PressButtonProtocol.swift
//  timeTable
//
//  Created by Павел Москалев on 10/1/21.
//

import Foundation


protocol pressReadyButtonProtocol: class {
    func readyButtonTapped( indexPath : IndexPath)
}
