//
//  UIStackView.swift
//  timeTable
//
//  Created by Павел Москалев on 9/24/21.
//

import UIKit


extension  UIStackView {
    convenience init(arrangedSubviews : [UIView], axis : NSLayoutConstraint.Axis, spacing : CGFloat, distribution : UIStackView.Distribution) {
        self.init(arrangedSubviews: arrangedSubviews)
        self.axis = axis
        self.spacing = spacing
        self.distribution = distribution
        self.translatesAutoresizingMaskIntoConstraints = false
    }
}
