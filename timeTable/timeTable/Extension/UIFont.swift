//
//  UIFont.swift
//  timeTable
//
//  Created by Павел Москалев on 9/28/21.
//

import UIKit


extension UIFont {
    
    static func  SFProText16() -> UIFont? {
        return UIFont.init(name: "SF Pro Text", size: 16)
    }
    
    static func  SFProText20() -> UIFont? {
        return UIFont.init(name: "SF Pro Text", size: 20)
    }
    
    static func  SFProDisplay20() -> UIFont? {
        return UIFont.init(name: "Helvetica Bold", size: 20)
    }
    
    static func  SFProDisplay16() -> UIFont? {
        return UIFont.init(name: "Helvetica", size: 14)
    }
    
}
