//
//  UILabel.swift
//  timeTable
//
//  Created by Павел Москалев on 9/24/21.
//
import UIKit


extension  UILabel {
    convenience init(text: String, font: UIFont?,textAlignment:  NSTextAlignment ) {
        self.init()
        self.text = text
        self.font = font
        self.textAlignment = textAlignment
        self.textColor = .black
        self.translatesAutoresizingMaskIntoConstraints = false
        self.adjustsFontSizeToFitWidth = true
    }
}


