//
//  TimeTableViewCell.swift
//  timeTable
//
//  Created by Павел Москалев on 9/2/21.
//

import UIKit

class TimeTableViewCell : UITableViewCell {
    
    var lessonName = UILabel(text: "Программирование", font: .SFProDisplay20(), textAlignment: .left)
    var teacherName = UILabel(text: "Илья Иванов Иванович", font: .SFProDisplay20(), textAlignment: .right)
    var lessonTime = UILabel(text: "08:00", font: .SFProDisplay20(), textAlignment: .left)
    var typeLabel = UILabel(text: "Тип", font: .SFProDisplay16(), textAlignment: .right)
    var lessonType = UILabel(text: "Лекция", font: .SFProDisplay20(), textAlignment: .left)
    var buildLabel = UILabel(text: "Корпус", font: .SFProDisplay16(), textAlignment: .right)
    var lessonBuild = UILabel(text: "1", font: .SFProDisplay20(), textAlignment: .left)
    var audLesson = UILabel(text: "Аудитория", font: .SFProDisplay16(), textAlignment: .right)
    var lessonAud = UILabel(text: "404", font: .SFProDisplay20(), textAlignment: .left)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setConstraintsStackView()
        self.selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        setConstraints()
    }
    
    func setConstraintsStackView() {
        
        self.addSubview(lessonTime)
        NSLayoutConstraint.activate([
                                        lessonTime.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
                                        lessonTime.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 5),
                                        lessonTime.widthAnchor.constraint(equalToConstant: 100),
                                        lessonTime.heightAnchor.constraint(equalToConstant: 25)])
        
        
        let topStackView = UIStackView(arrangedSubviews: [lessonName, teacherName], axis: .horizontal, spacing: 10, distribution: .fillEqually)
        self.addSubview(topStackView)
        NSLayoutConstraint.activate([
                                        topStackView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
                                        topStackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 5),
                                        topStackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5),
                                        topStackView.heightAnchor.constraint(equalToConstant: 25)])
        
        let bottomStackView = UIStackView(arrangedSubviews: [typeLabel, lessonType, buildLabel, lessonBuild, audLesson, lessonAud ], axis: .horizontal, spacing: 5, distribution: .fillProportionally)
        self.addSubview(bottomStackView)
        NSLayoutConstraint.activate([
                                        bottomStackView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
                                        bottomStackView.leadingAnchor.constraint(equalTo: self.lessonTime.trailingAnchor, constant: 5),
                                        bottomStackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5),
                                        bottomStackView.heightAnchor.constraint(equalToConstant: 25)])
        
        
    }
}
func setConstraints() {}
