//
//  AddNewLessonTableViewCell.swift
//  timeTable
//
//  Created by Павел Москалев on 10/5/21.
//



import UIKit

class AddNewLessonTableViewCell : UITableViewCell {
    
    
    let backgroundViewCell : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    let nameCellLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        return label
    }()
    
    let repeatWeekSwitch: UISwitch = {
       let repeatSwitch = UISwitch()
        repeatSwitch.isOn = true
        repeatSwitch.isHidden = true
        repeatSwitch.tintColor = .green
        repeatSwitch.translatesAutoresizingMaskIntoConstraints = false
        return repeatSwitch
    }()
    
    let cellNameArray = [["Дата","Время"],
                         ["Название","Тип","Корпус","Аудитория"],
                         ["Имя преподавателя"],
                         [""],
                         ["Повторяем каждую неделю?"]]
    
    
    let textFieldAlertArray = [["Дата","Время"],
                         ["Введите название","Укажите тип","Номер Корпуса","Номер Аудитории"],
                         ["Имя преподавателя"],
                         [""],
                         ["Повторяем каждую неделю?"]]
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setConstraints()
        self.selectionStyle = .none
        self.backgroundColor = .clear
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
    }
    
    
    func prepareCell(indexPath: IndexPath){
        nameCellLabel.text = cellNameArray[indexPath.section][indexPath.row]
        if indexPath == [3,0]
        {
            backgroundViewCell.backgroundColor = .green
        }
        if indexPath == [4,0]
        {
            repeatWeekSwitch.isHidden = false
        }
    }
    
    func preparePlaceholder (indexPath: IndexPath){
        nameCellLabel.text = cellNameArray[indexPath.section][indexPath.row]
    }
    
    func setConstraints() {
        
        self.addSubview(backgroundViewCell)
        NSLayoutConstraint.activate([
                                        backgroundViewCell.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
                                        backgroundViewCell.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
                                        backgroundViewCell.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),backgroundViewCell.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -1)])
        
        self.addSubview(nameCellLabel)
        NSLayoutConstraint.activate([
                                        nameCellLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0),
                                        nameCellLabel.leadingAnchor.constraint(equalTo: self.backgroundViewCell.leadingAnchor, constant: 10)])
        
        self.contentView.addSubview(repeatWeekSwitch)
        NSLayoutConstraint.activate([
                                        repeatWeekSwitch.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0),
                                        repeatWeekSwitch.trailingAnchor.constraint(equalTo: self.backgroundViewCell.trailingAnchor, constant: -10)])
    }
}
