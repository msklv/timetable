//
//  NewHomeWorkTableViewCell.swift
//  timeTable
//
//  Created by Павел Москалев on 10/8/21.
//

import UIKit

class NewHomeWorkTableViewCell : UITableViewCell {
    
    
    let backgroundViewCell : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    let nameCellLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        return label
    }()
    
    let cellNameArray = ["Дата","Название предмета","Введите задание",""]
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setConstraints()
        self.selectionStyle = .none
        self.backgroundColor = .clear
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
    }
    
    
    func prepareCell(indexPath: IndexPath){
        nameCellLabel.text = cellNameArray[indexPath.section]
        if indexPath == [3,0]
        {
            backgroundViewCell.backgroundColor = .green
        }
    }
    
    func setConstraints() {
        
        self.addSubview(backgroundViewCell)
        NSLayoutConstraint.activate([
                                        backgroundViewCell.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
                                        backgroundViewCell.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
                                        backgroundViewCell.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),backgroundViewCell.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -1)])
        
        self.addSubview(nameCellLabel)
        NSLayoutConstraint.activate([
                                        nameCellLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0),
                                        nameCellLabel.leadingAnchor.constraint(equalTo: self.backgroundViewCell.leadingAnchor, constant: 10)])
        
    }
}

