//
//  HomeWorkTableViewCell.swift
//  timeTable
//
//  Created by Павел Москалев on 10/1/21.
//


import UIKit

class HomeWorkTableViewCell : UITableViewCell {
    
    var taskName = UILabel(text: "Программирование", font: .SFProText20(), textAlignment: .left)
    var taskDescription = UILabel(text: "Читать вторую главу по комбайну", font: .SFProDisplay16(), textAlignment: .left)
    
    var readyButton : UIButton = {
        var button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setBackgroundImage(UIImage.init(systemName: "chevron.down.circle"), for: .normal)
        button.tintColor = .black
        
        return button
    }()
    
    weak var cellHomeWorkDelegate: pressReadyButtonProtocol?
    var index : IndexPath?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        taskDescription.numberOfLines = 2
        setConstraints()
        readyButton.addTarget(self, action: #selector(readyButtonTapped), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func readyButtonTapped() {
        guard let index = index else { return }
        cellHomeWorkDelegate?.readyButtonTapped(indexPath: index)
    }
    
    func setConstraints()
    {
        self.contentView.addSubview(readyButton)
        NSLayoutConstraint.activate(
            [readyButton.centerYAnchor.constraint(equalTo: self.centerYAnchor),
             readyButton.trailingAnchor.constraint(equalTo: self.trailingAnchor , constant: -20),
             readyButton.heightAnchor.constraint(equalToConstant: 40),
             readyButton.widthAnchor.constraint(equalToConstant: 40),
            ])
        
        self.addSubview(taskName)
        NSLayoutConstraint.activate(
            [taskName.topAnchor.constraint(equalTo: self.topAnchor , constant: 10),
             taskName.trailingAnchor.constraint(equalTo: readyButton.leadingAnchor , constant: -5 ),
             taskName.leadingAnchor.constraint(equalTo: self.leadingAnchor  ,constant: 5),
             taskName.heightAnchor.constraint(equalToConstant: 25),
            ])
        
        self.addSubview(taskDescription)
        NSLayoutConstraint.activate(
            [taskDescription.topAnchor.constraint(equalTo: taskName.bottomAnchor , constant: 5),
             taskDescription.trailingAnchor.constraint(equalTo: readyButton.leadingAnchor , constant: -5 ),
             taskDescription.leadingAnchor.constraint(equalTo: self.leadingAnchor  ,constant: 5),
             taskDescription.bottomAnchor.constraint(equalTo: self.bottomAnchor ,constant: -5),
            ])
    }
}
