//
//  AlertWithTextField.swift
//  timeTable
//
//  Created by Павел Москалев on 10/7/21.
//

import UIKit

extension UIViewController {
    
    func alertWithText( label : UILabel, name : String, placeholder: String) {
        let alert = UIAlertController(title: name, message: nil, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Ok", style: .default) { (action) in
            let alertTextField = alert.textFields?.first
            guard let text = alertTextField?.text else { return }
            label.text = text
        }
        
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = placeholder
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alert.addAction(ok)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)

    }
}
