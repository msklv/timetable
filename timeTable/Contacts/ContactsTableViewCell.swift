//
//  ContactsTableViewCell.swift
//  timeTable
//
//  Created by Павел Москалев on 10/8/21.
//

import UIKit

class ContactsTableViewCell : UITableViewCell {
    
   
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setConstraints()
    {
       
    }
}
